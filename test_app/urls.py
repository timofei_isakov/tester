# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from .views import LoginView, view_logout, TestAddView, MyTestsView, TestsView, TestEditView, TestEditQView, \
    TestQView, TestAddQView, TestView, TestSuccessView

urlpatterns = [
    # Examples:
    # url(r'^$', 'IoT.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url('login/$', LoginView.as_view()),
    url('logout/$', view_logout),
    url('tests/add/$', TestAddView.as_view()),
    url('tests/edit/(?P<pk>\d+)/$', TestEditView.as_view()),
    url('tests/edit/(?P<pk>\d+)/questions/$', TestQView.as_view()),
    url('tests/edit/(?P<pk>\d+)/questions/add/$', TestAddQView.as_view()),
    url('tests/edit/(?P<pk>\d+)/questions/(?P<q_id>\d+)/$', TestEditQView.as_view()),
    url('tests/my/$', MyTestsView.as_view()),
    url('^tests/$', TestsView.as_view()),
    url('^test/(?P<test_id>\d+)/start/$', TestView.as_view()),
    url('^test/(?P<test_id>\d+)/success/$', TestSuccessView.as_view()),
    url('^$', TestsView.as_view()),
]
