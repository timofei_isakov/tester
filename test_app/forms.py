# -*- coding: utf-8 -*-
from django import forms
from django.forms import Textarea

from .models import Testing, Question, Answer, UserAnswer

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, label="")
    password = forms.CharField(max_length=50, widget=forms.PasswordInput, label="")

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = u'Логин'
        self.fields['username'].widget.attrs['id'] = u'inputEmail'
        self.fields['username'].widget.attrs['class'] = u'form-control input-lg'
        self.fields['password'].widget.attrs['placeholder'] = u'Пароль'
        self.fields['password'].widget.attrs['type'] = u'password'
        self.fields['password'].widget.attrs['class'] = u'form-control input-lg'


class TestForm(forms.Form):

    name = forms.CharField(max_length=100)
    description = forms.CharField(max_length=1000, widget=Textarea)
    pass_score = forms.IntegerField(required=False, min_value=0)
    question_count = forms.IntegerField(min_value=1)

    def __init__(self, *args, **kwargs):
        super(TestForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = u'Название теста'
        self.fields['name'].widget.attrs['class'] = u'form-control input-lg'
        self.fields['description'].widget.attrs['placeholder'] = u'Описание теста'
        self.fields['description'].widget.attrs['class'] = u'form-control input-lg'
        self.fields['pass_score'].widget.attrs['placeholder'] = u'Проходной балл'
        self.fields['pass_score'].widget.attrs['class'] = u'form-control input-lg'
        self.fields['question_count'].widget.attrs['placeholder'] = u'Число вопросов в тесте'
        self.fields['question_count'].widget.attrs['class'] = u'form-control input-lg'


class TestEditForm(forms.ModelForm):

    class Meta:
        model = Testing
        exclude = ('author', 'creation_date', 'manual_check', 'max_score')

    def __init__(self, *args, **kwargs):
        super(TestEditForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = u'form-control input-lg'


class TestAddQForm(forms.ModelForm):

    class Meta:
        model = Question
        exclude = ('testing', 'description')

    def __init__(self, *args, **kwargs):
        super(TestAddQForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = u'form-control input-lg'


class TestAddAnswerForm(forms.ModelForm):

    class Meta:
        model = Answer
        exclude = ('question', )

    def __init__(self, *args, **kwargs):
        super(TestAddAnswerForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = u'form-control input-lg'


class OneOfManyAnswerForm(forms.ModelForm):
    answers = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, *args, **kwargs):
        super(OneOfManyAnswerForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = u'form-control input-lg'


    class Meta:
        model = Answer
        exclude = ('user', 'question')


class MyCustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        if 'my_choices' in kwargs:
            choices = kwargs.pop('my_choices')
        else:
            choices = None
        super(MyCustomForm, self).__init__(*args, **kwargs)
        self.fields["answer"] = forms.ChoiceField(choices=choices, widget=forms.CheckboxSelectMultiple)