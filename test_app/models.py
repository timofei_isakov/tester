# encoding: utf-8
import hashlib
import random
from django.db import models
from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User


ANSWER_LENGTH = 1000

class Testing(models.Model):
    CLOSED = 'closed'
    ACTIVE = 'active'
    DRAFT = 'draft'
    STATUS = (
        (DRAFT, u'Черновик'),
        (ACTIVE, u'Активна'),
        (CLOSED, u'Закрыта'),
    )

    NO_AUTH = 'no_auth'
    AUTH = 'auth'
    TYPE_AUTH = (
        (NO_AUTH, u'Не авторизованные'),
        (AUTH, u'Авторизованные')
    )

    name = models.CharField(u'Название', max_length=512)
    creation_date = models.DateTimeField(u'Дата создания', auto_now_add=True)
    author = models.ForeignKey(User, verbose_name=u'Логин пользователя')
    description = models.TextField(max_length=1000, blank=True, verbose_name=u"Описание")
    status = models.CharField(u'Статус', choices=STATUS, max_length=64, default=DRAFT)
    manual_check = models.BooleanField(default=True, blank=True, verbose_name=u"Ручная проверка")
    max_score = models.PositiveIntegerField(default=0, blank=True, verbose_name=u"Максимальное количество баллов")
    pass_score = models.PositiveIntegerField(default=0, verbose_name=u"Проходной балл")
    question_count = models.PositiveIntegerField(default=0, verbose_name=u"Количество вопросов в тесте")

    class Meta:
        verbose_name = u'тест'
        verbose_name_plural = u'тесты'
        ordering = ('creation_date',)

    def __unicode__(self):
        return self.name

    def can_edit(self):
        if self.status in [self.ACTIVE, self.CLOSED]:
            return False
        return True

    def can_change_status(self):
        return not (self.status == self.CLOSED)

    def get_valid_questions(self):
        return [question for question in self.questions.all() if question.check()]

    def check(self):
        for question in self.questions.all():
            if not question.check():
                return False
        return True if self.questions.all() else False

    def get_status(self, id):
        status = Testing.STATUS[int(id)]
        return {'id': status[0], 'name': status[1]}

    def get_answered_customers(self):
        return self.customers.filter(failure=False)

    def check_show_auth_user(self, auth):
        if not self.survey_type_auth:
            return True
        elif self.survey_type_auth == self.AUTH and auth:
            return True
        elif self.survey_type_auth == self.NO_AUTH and not auth:
            return True
        return False

    def check_show_rate_user(self, rate):
        if not self.survey_rate:
            return True
        elif rate and self.survey_rate == rate:
            return True
        return False

    def check_survey_can_show_user(self, auth, ip=None, rate=None):
        if self.check_show_auth_user(auth) and self.check_show_rate_user(rate) and self.check_show_ip_range(ip):
            return True
        else:
            return False


class Question(models.Model):
    MULTIPLE_CHOICE = 'multiple'
    SINGLE_CHOICE = 'single'
    ESTABLISHING_COMPLIANCE = 'establishing compliance'
    SEQUENCING = 'sequencing'
    IMMUNE_ANSWER = 'immunw answer'
    CHOICE_TYPE = (
        (SINGLE_CHOICE, u'Одиночный выбор'),
        (MULTIPLE_CHOICE, u'Множественный выбор'),
        (ESTABLISHING_COMPLIANCE, u'Установление соответствия'),
        (SEQUENCING, u'Установление последовательности'),
        (IMMUNE_ANSWER, u'Свободный ответ'),
    )

    TEXT_TYPE = 'text'
    SELECT_TYPE = 'select'
    MIXED_TYPE = 'mix'
    ANSWER_TYPE = (
        (TEXT_TYPE, u'Текстовый ответ'),
        (SELECT_TYPE, u'Выбор из списка'),
        (MIXED_TYPE, u'Смешанный ответ'),
    )
    testing = models.ForeignKey(Testing, verbose_name=u'Тест', related_name='questions')
    question_number = models.IntegerField(u'Номер вопроса')
    choice_type = models.CharField(u'Тип выбора', choices=CHOICE_TYPE, max_length=64)
    answer_type = models.CharField(u'Тип ответа', choices=ANSWER_TYPE, max_length=64)
    question = models.TextField(u'Вопрос', max_length=ANSWER_LENGTH)
    description = models.TextField(verbose_name=u'Комментарий', null=True, blank=True)
    plus_points = models.PositiveIntegerField(default=0, verbose_name=u"Баллов за правильный ответ")
    minus_points = models.PositiveIntegerField(default=0, verbose_name=u"Минус баллов за неправильный ответ*")
    max_points = models.PositiveIntegerField(default=0, verbose_name=u"Максимальное число очков**")

    class Meta:
        verbose_name = u'вопрос'
        verbose_name_plural = u'вопросы'
        ordering = ('question_number',)

    def __unicode__(self):
        return '%s' % self.question

    def check(self):
        return not (self.answer_type in [self.SELECT_TYPE, self.MIXED_TYPE] and self.choice_type in [self.MULTIPLE_CHOICE,
                                                                                                     self.SINGLE_CHOICE] and self.answers.count() == 0)

    def get_answer_not_answerlist(self):
        return self.question_values.exclude(answer__isnull=False)


class Answer(models.Model):
    question = models.ForeignKey(Question, verbose_name=u'Вопрос', related_name='answers')
    answer = models.CharField(u'Ответ', max_length=ANSWER_LENGTH)
    num_answer = models.PositiveIntegerField(default=0, verbose_name=u'Номер ответа')
    correct = models.BooleanField(default=True, verbose_name=u'Правильность ответа')

    class Meta:
        verbose_name = u'предлагаемый ответ'
        verbose_name_plural = u'предлагаемые ответы'
        ordering = ('question', 'num_answer', 'correct')


    def __unicode__(self):
        return '%s' % self.answer


class UserAnswer(models.Model):
    user = models.ForeignKey(User, related_name='answers', )
    question = models.ForeignKey(Question, verbose_name=u'вопрос', related_name='customer_answers')
    value = models.CharField(u'ответ', max_length=ANSWER_LENGTH)

    class Meta:
        verbose_name = u'Ответ пользователя'
        verbose_name_plural = u'Ответы пользователей'

    def __unicode__(self):
        return '%s-%s' % (self.question.question_number, self.values.all().values_list('pk', flat=True))

    def get_values(self):
        return self.values.all()

    def get_values_answer_list(self):
        return [value.get_answer() for value in self.get_values()]
