# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect

# Create your views here.
# -*- coding: utf-8 -*-
from django.shortcuts import render
from .forms import LoginForm, TestForm, TestEditForm, TestAddQForm, TestAddAnswerForm, OneOfManyAnswerForm, MyCustomForm
from django.contrib import auth
from django.http import HttpResponseRedirect, Http404, HttpResponseBadRequest, HttpResponse
from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, UpdateView, CreateView

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import json
import random
from .models import Testing, Question, Answer, UserAnswer
# Create your views here.

class LoginView(View):
    def get(self, request):
        if request.user.is_authenticated():
            raise Http404()

        login_form = LoginForm()
        return render(request, 'login.html', {'login_form': login_form,})

    def post(self, request):
        if request.user.is_authenticated():
            raise Http404()

        login_form = LoginForm(request.POST)
        error = ''


        if login_form.is_valid():
            cd = login_form.cleaned_data

            user = auth.authenticate(username=cd['username'], password=cd['password'])

            if user is not None and user.is_active:
                auth.login(request, user)
                return HttpResponseRedirect('/')
            else:
                error = 'Войти в систему не удалось. Проверьте введенные данные'



        return render(request, 'login.html', {'login_form': login_form,
                                              'error': error})


def view_logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


class TestAddView(FormView):
    template_name = "add_test.html"
    form_class = TestForm
    success_url = '/tests/my/'
    request = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        # self.request = request
        if not request.user.is_superuser:
            return redirect('/tests/')
        return super(TestAddView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        cd = form.cleaned_data
        test = Testing()
        test.author = self.request.user
        test.name = cd['name']
        test.description = cd['description']
        test.pass_score = cd['pass_score']
        test.question_count = cd['question_count']
        test.save()
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(TestAddView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context


class MyTestsView(ListView):
    template_name = 'my_tests.html'
    model = Testing
    paginate_by = 20
    context_object_name = 'tests'
    request = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect('/tests/')
        return super(MyTestsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MyTestsView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context

    def get_queryset(self):

        return Testing.objects.filter(author=self.request.user)


class TestsView(ListView):
    template_name = 'tests.html'
    model = Testing
    paginate_by = 20
    context_object_name = 'tests'
    request = None

    # @method_decorator(login_required(login_url='/login/'))
    # def dispatch(self, request, *args, **kwargs):
    #
    #     return super(TestsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TestsView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context

    def get_queryset(self):

        return Testing.objects.filter(status=Testing.ACTIVE)


class TestEditView(UpdateView):

    template_name = "test_edit.html"
    form_class = TestEditForm
    success_url = '/tests/my/'
    request = None
    model = Testing

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        try:
            Testing.objects.get(author=request.user, id=kwargs['pk'])
        except:
            return redirect('/tests/')
        return super(TestEditView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TestEditView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context


class TestQView(ListView):
    model = Question
    paginate_by = 30
    context_object_name = 'questions'
    template_name = 'question_list.html'
    request = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect('/tests/')
        return super(TestQView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TestQView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context

    def get_queryset(self):
        return Question.objects.filter(testing=self.kwargs['pk'], testing__author=self.request.user)


class TestAddQView(FormView):

    model = Question
    template_name = "add_question.html"
    fields = ['question_number', ]
    form_class = TestAddQForm

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect('/tests/')
        try:
            Testing.objects.get(author=request.user, id=kwargs['pk'])
        except:
            return redirect('/tests/')
        return super(TestAddQView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        cd = form.cleaned_data
        test = Testing.objects.get(author=self.request.user, id=self.kwargs['pk'])
        question = Question()
        question.testing = test
        question.question_number = cd['question_number']
        question.question = cd['question']
        question.choice_type = cd['choice_type']
        question.answer_type = cd['answer_type']
        question.plus_points = cd['plus_points']
        question.minus_points = cd['minus_points']
        question.max_points = cd['max_points']
        question.save()
        return redirect('../')


class TestEditQView(View):

    question = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        try:
            self.question = Question.objects.get(id=kwargs['q_id'], testing__author=request.user)
        except Question.DoesNotExist:
            return redirect("/tests/my/")
        return super(TestEditQView, self).dispatch(request, *args, **kwargs)

    def get(self, request, pk, q_id):

        edit_q_form = TestAddQForm(initial={
            'question_number': self.question.question_number,
            'choice_type': self.question.choice_type,
            'answer_type': self.question.answer_type,
            'question': self.question.question,
            'max_points': self.question.max_points,
            'minus_points': self.question.minus_points,
            'plus_points': self.question.plus_points
        })
        answers = Answer.objects.filter(question=q_id)
        add_answer_form = TestAddAnswerForm()
        return render(request, 'question_edit.html', {
            'request': request,
            'question': self.question, 'edit_q_form': edit_q_form,
            'answers': answers, 'add_answer_form': add_answer_form
            })

    def post(self, request, pk, q_id):
        add_answer_form = TestAddAnswerForm(request.POST)
        edit_q_form = TestAddQForm(request.POST)
        if edit_q_form.is_valid():
            self.save_question(edit_q_form)
        if add_answer_form.is_valid():
            self.save_answer(add_answer_form)

        return self.get(request, pk, q_id)

    def save_question(self, form):
        cd = form.cleaned_data
        self.question.question = cd['question']
        self.question.choice_type = cd['choice_type']
        self.question.question_number = cd['question_number']
        self.question.answer_type = cd['answer_type']
        self.question.minus_points = cd['minus_points']
        self.question.plus_points = cd['plus_points']
        self.question.max_points = cd['max_points']
        self.question.save()

    def save_answer(self, form):
        cd = form.cleaned_data
        if self.question.choice_type == self.question.SINGLE_CHOICE and bool(cd['correct']):
            count = Answer.objects.filter(question=self.question).count()
            if count > 0:
                return False
        answer = Answer()
        answer.question = self.question
        answer.answer = cd['answer']
        answer.correct = cd['correct']
        answer.num_answer = cd['num_answer']
        answer.save()


class TestView(View):

    test = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        try:
            self.test = Testing.objects.get(id=kwargs['test_id'])
        except Question.DoesNotExist:
            return redirect("/tests/")
        return super(TestView, self).dispatch(request, *args, **kwargs)

    def get(self, request, test_id):
        points = 0
        step = 0
        if 'max' not in request.session:
            answers_count = UserAnswer.objects.filter(question__testing__id=test_id, user=request.user).count()
            if answers_count > 0:
                points = self.recount_result(request)
            question_count = Question.objects.filter(testing=self.test).count()
            if question_count < self.test.question_count:
                request.session['max'] = question_count
            else:
                request.session['max'] = self.test.question_count
            request.session['points'] = 0
            step =request.session['step'] = 1
        else:
            step = request.session['step']
        if step >= request.session['max']:
            return redirect('/test/%d/success/' % int(test_id))

        question = random.choice(Question.objects.filter(testing__id=test_id, question_number=step))
        request.session['question'] = question.id
        answers = Answer.objects.filter(question=question)


        return render(request, 'test_view.html', {
            "points": points, "step": step,
            "question": question,
            "answers": answers
        })

    def post(self, request, test_id):
        question_id = request.session['question']
        try:
            question = Question.objects.get(id=question_id)
        except:
            return self.get(request, test_id)
        else:
            answers = Answer.objects.filter(question=question, correct=True)
            if 'answer' in request.POST:
                user_answers = []
                for i in request.POST.getlist('answer'):
                    user_answers.append(int(i))
                for answer in answers:
                    if answer.id in user_answers:
                        request.session['points'] += question.plus_points
                        user_answers.remove(answer.id)
                    else:
                        request.session['points'] -= question.minus_points
                if len(user_answers) > 0:
                    request.session['points'] -= question.minus_points* len(user_answers)
                request.session['step'] += 1
        return self.get(request, test_id)



    def recount_result(self, request):
        points = 0
        answers_count = UserAnswer.objects.filter(user=request.user, question__testing=self.test).count()
        if answers_count == 0:
            return False
        # correct_answers = UserAnswer.objects.filter(question__testing=self.test, correct=True)
        questions = Question.objects.filter(testing=self.test)
        for question in questions:
            user_answer_count = UserAnswer(question=question, user=request.user).count()
            if user_answer_count < 1:
                break
            if question.choice_type == Question.SINGLE_CHOICE:
                try:
                    answer = Question.objects.get(question=question)
                    user_answer = UserAnswer(question=question, user=request.user)
                except:
                    break
                if answer.answer == user_answer.value:
                    points += question.plus_points

            elif question.choice_type == Question.MULTIPLE_CHOICE:
                answers = Question.objects.filter(question=question).values('answer')
                user_answer = UserAnswer.objects.filter(question=question, user=request.user).values('values')

                for answer in answers:
                    if answer in user_answer:
                        points += question.plus_points
                    else:
                        points -= question.minus_points
            elif question.choice_type == Question.SEQUENCING:
                pass
            elif question.choice_type == Question.IMMUNE_ANSWER:
                pass

        return points

class TestSuccessView(View):

    test = None

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        try:
            self.test = Testing.objects.get(id=kwargs['test_id'])
        except Question.DoesNotExist:
            return redirect("/tests/")
        if 'points' not in request.session or 'max' not in request.session or\
                                'step' not in request.session or request.session['max'] - request.session['step'] != 0:
            return redirect("/tests/")
        return super(TestSuccessView, self).dispatch(request, *args, **kwargs)

    def get(self, request, test_id):
        return render(request, "test_success.html", {
            "request": request, "test": self.test, "points": request.session['points']
        })
