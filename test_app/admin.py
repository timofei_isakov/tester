from django.contrib import admin

from .models import Testing, Answer, UserAnswer, Question

# Register your models here.
admin.site.register(Testing)
admin.site.register(Answer)
admin.site.register(UserAnswer)
admin.site.register(Question)