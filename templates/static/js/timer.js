document.addEventListener("DOMContentLoaded", function() {
    var eventTime = parseInt(document.getElementById("event").textContent, 10);

    var intervalID = window.setInterval(function() {
        var text = '';
        //
        var secondsToEvent = eventTime - (new Date).getTime()/1000;
        // days
        var days = parseInt(secondsToEvent / (60 * 60 * 24), 10);
        if(days > 0) {
            text += days + "дн ";
            secondsToEvent -= days * 60 * 60 * 24;
        }
        // hours
        var hours = parseInt(secondsToEvent / (60 * 60), 10);
        if(hours > 0) {
            text += hours + "ч ";
            secondsToEvent -= hours * 60 * 60;
        }
        // minutes
        var minutes = parseInt(secondsToEvent / 60, 10);
        if(minutes > 0) {
            text += minutes + "мин ";
            secondsToEvent -= minutes * 60;
        }
        // seconds
        if(secondsToEvent > 0) {
            text += parseInt(secondsToEvent, 10) + "сек ";
        }

        if(text != '') {
            document.getElementById("timer").textContent = text;
        } else {
            document.getElementById("timer").textContent = '';
        }
    }, 1000);
});