
$.ajaxSetup({
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     } 
});


$(document).ready(function(){
	$("#nav_left").css({"height":$("#middle").outerHeight()});
	
	$("#nav_left").hover(function(){
		$("#container").innerWidth($("#container").innerWidth() - 240);
		/*$('#nav_left,#container').clearQueue().stop();
		$("#container").animate({"width":$("#container").width() - 240}, 1000);
		$("#nav_left").animate({"width":"300px"}, 1000);*/
	},
	function(){
		$("#container").innerWidth($("#container").innerWidth() + 240);
		/*$('#nav_left,#container').clearQueue().stop();
		$("#nav_left").animate({"width":"60px"}, 1000);
		$("#container").animate({"width":$("#container").width() + 240}, 1000);*/
	});
	$("#add_hardware_btn").click(function(){
	    $('#add_hardware_btn').hide();
        $('#add_hardware').show();

	});

});